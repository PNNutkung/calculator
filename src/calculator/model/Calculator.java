package calculator.model;

/**
 * The logic component for a calculator.
 * This calculator has 2 registers for saving data: reg1 and reg2.
 * reg1 is the first operand and also stores results of binary operations.
 * reg2 is the second operand for binary operations.
 * currentRegister is 1 or 2 if the "current" register for data is reg1 or reg2.
 * Example:
 * setValue(12)        reg1=12
 * setOperand('+')     operand = +, 
 *                     currentRegister=2
 * setValue(9)         reg2=9
 * perform( )          reg1= reg1 operand reg2 = 12+9
 *                     currentRegister=1
 * 
 * This is a deliberately bad design.
 * Apply some modeling and design patterns to make it better.
 */
public class Calculator extends java.util.Observable {
	private static final char NOOP = ' '; // no operation pending
	private double reg1 = 0;
	private double reg2 = 0;
	private int currentRegister = 1;
	//private Operator op = new calculator.operator.Noop();
	private char operator = NOOP; // no operator yet
	
	public Calculator( ) {
		reg1 = reg2 = 0.0;
		currentRegister = 1;
		operator = NOOP;
	}
	
	/**
	 * Set the value of a register, depending on state of calculator.
	 * @param value is the value to save in register
	 */
	public void setValue(double value) {
		if (currentRegister==1) reg1 = value;
		else reg2 = value;
	}
	
    /**
	 * Get the value of the current register.
	 * @return value of current register
	 */
	public double getValue( ) {
		if (currentRegister==1) return reg1;
		else return reg2;
	}
	
	/**
	 * The the operation to perform, but don't actually perform it yet.
	 * The operation is performed when perform() is invoked.
	 * This enables handling of infix notation, such as 2 + 3.
	 * @param operator is the operator to be performed
	 */
	public void setOperator(char operator) {
		//TODO perform saved operator before setting a new operator?
		// if calculator is already in the register2 'state' then it
		// should first perform a previously entered operation, so that
		// complex expressions will work, like 2 + 4 * 5 - 1.

		this.operator = operator;
		// after an operand is input, the next value always goes in register 2
		currentRegister = 2;
	}
	
	public char getOperator() {
		return operator;
	}
	
	/**
	 * Perform operation, if any
	 */
	public void perform( ) {
		if (operator == NOOP) return;
		switch(operator) {
		case '+': reg1 = reg1 + reg2; break;
		case '-': reg1 = reg1 - reg2; break;
		case '*': reg1 = reg1 * reg2; break;
		case '/': reg1 = reg1 / reg2; break;
		case '^': reg1 = Math.pow(reg1, reg2); break;
		default: System.out.println("Unrecognized operator: "+operator);
		}
		// after performing "=" the calculator state should be set
		// for a new operation, so next operand will be saved in register 1.
		currentRegister = 1;
		//TODO notify observers
	}
}
